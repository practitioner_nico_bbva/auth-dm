import { LitElement } from 'lit-element';
import { setHeader, setSession, removeSession, getToken } from '@practitioner/security-module';
import '@cells-components/cells-generic-dp';

/**
This component es utilizado para autenticación y creación de usuarios.

Example:

```html
<auth-dm></auth-dm>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class AuthDm extends LitElement {
  static get is() {
    return 'auth-dm';
  }

  // Declare properties
  static get properties() {
    return {
      host: String
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.host = "http://localhost:1122";
  }

  /**
   * 
   * @param {Object} detail Datos necesarios para crear el usuario.
   * Este método utiliza el módulo de seguridad para enviar en base64 dentro de la cabecera los datos: mail, password y user.
   * Instancia un data provider y lo ejecuta.
   */
  createUser(detail) {
    this._emmit('service_loaded', 'display-block');
    const DP = customElements.get('cells-generic-dp');

    let dp = new DP();
    dp.headers = setHeader('Authorization', `${detail.mail}:${detail.password}:${detail.user}`);
    dp.host = this.host;
    dp.path = "auth/users/signup"
    dp.method = "POST"
    dp.timeout = "10000"
    dp.generateRequest()
      .then(this._createFinally.bind(this))
      .catch(this._createFinally.bind(this));
  }

  /**
   * 
   * @param {Object} detail Datos necesarios para logear el usuario.
   * Este método utiliza el módulo de seguridad para enviar en base64 dentro de la cabecera los datos: mail y password.
   * Instancia un data provider y lo ejecuta.
   */
  loginUser(detail) {
    this._emmit('service_loaded', 'display-block');
    const DP = customElements.get('cells-generic-dp');

    let dp = new DP();
    dp.headers = setHeader('Authorization', `${detail.mail}:${detail.password}`, 'Basic')
    dp.host = this.host;
    dp.path = "auth/users/access"
    dp.method = "POST"
    dp.timeout = "10000"
    dp.generateRequest()
      .then(this._signInSuccess.bind(this))
      .catch(this._createFinally.bind(this));
  }

  /**
   * 
   * @param {String} eventName nombre del evento
   * @param {*} detail Detalle a  enviar.
   * Es un método de utilidad. 
   */
  _emmit(eventName, detail) {
    this.dispatchEvent(new CustomEvent(eventName, { detail, composed: true, bubbles: true }));
  }

  /**
   * Es el callback de la respuesta satisfactoria.
   * Emite eventos con el detalle y estado.
   * @param {Object} res la respuesta del servicio
   */
  _signInSuccess(res) {
    setSession(res.response.token);
    window.history.back();
    this._emmit('login_success', res.response.message);
    this._emmit('service_loaded', 'fade-out');
    this.dispatchEvent(new CustomEvent('session_state', { detail: "coronita:on", composed: true, bubbles: true }));
  }

  /**
   * Es el callback de la respuesta de error.
   * Emite eventos con el detalle y estado.
   * @param {Object} res la respuesta del servicio
   */
  _createFinally(res) {
    this._emmit('message_response', res.response.message);
    this._emmit('show_message', true);
    this._emmit('service_loaded', 'fade-out');
  }

  /**
   * Método utilziado para modificar el icono de la barra del header dependiendo de la sesión.
   */
  validateSession() {
    if( getToken() ) {
      this.dispatchEvent(new CustomEvent('session_state', { detail: "coronita:on", composed: true, bubbles: true }));
    } else {
      this.dispatchEvent(new CustomEvent('session_state', { detail: false, composed: true, bubbles: true }));
    }
  }

  /**
   * Cierra sesión y lo informa.
   */
  logOut() {
    removeSession();
    this.dispatchEvent(new CustomEvent('session_state', { detail: false, composed: true, bubbles: true }));
    this.dispatchEvent(new CustomEvent('close_alert', { detail: false, composed: true, bubbles: true }));
  }
}

// Register the element with the browser
customElements.define(AuthDm.is, AuthDm);
